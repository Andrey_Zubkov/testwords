<?php

namespace app\models;

use yii\data\ActiveDataProvider;

class WordRecordSearch extends WordRecord
{
    public $dateFrom;
    public $dateTo;

    public function rules()
    {
        return [
            [['user_ip', 'dateFrom', 'dateTo'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = WordRecord::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'user_ip', $this->user_ip]);
        if (!empty($this->dateFrom) && !empty($this->dateTo)) {
            $query->andFilterWhere(['between', 'created_at', strtotime($this->dateFrom), strtotime($this->dateTo) ]);
        }

        return $dataProvider;
    }
}