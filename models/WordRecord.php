<?php
namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;
/**
 * Class Word
 * @property integer $id
 * @property string $user_ip
 *
 */
class WordRecord extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_ip' => 'IP Адресс пользователя',
            'word' => 'Слово',
            'created_at' => 'Дата добавления'
        ];
    }

    public function beforeSave($insert)
    {
        $this->user_ip = Yii::$app->getRequest()->getUserIP();
        return parent::beforeSave($insert);
    }

    public static function tableName()
    {
        return '{{%words}}';
    }

    public function rules()
    {
        return [
            [['user_ip', 'word'], 'required']
        ];
    }

}