<?php
namespace app\models;

use yii\base\Model;


class DeclensionWords extends Model
{
    public $group;
    public $type;
    public $word;

    const GROUP_FIRST = 'first';
    const GROUP_SECOND = 'second';
    const GROUP_THIRD = 'third';

    const TYPE_ADJECTIVE = 'adjective';
    const TYPE_NONE = 'none';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['word'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function getRules()
    {
        return [
            self::GROUP_FIRST => [
                self::TYPE_ADJECTIVE => [
                    'suffixes' => [
                        [
                            'words' => ['шая'],
                            'graduation' => ['--ей', '--ей', '--ую', '--ей', '--ей']
                        ],
                        [
                            'words' => ['ая'],
                            'graduation' => ['---ой', '--ой', '--ую', '--ой', '--ой']
                        ],
                        [
                            'words' => ['яя'],
                            'graduation' => ['--ей', '--ей', '--юю', '--ей', '--ей']
                        ],
                        [
                            'words' => ['ые', 'ие'],
                            'graduation' => ['х', '-м', '.', '-ми', '-х']
                        ],
                    ]
                ],
                self::TYPE_NONE => [
                    'suffixes' => [
                        [
                            'words' => ['га', 'ка', 'ха', 'ча', 'ша', 'ща'],
                            'graduation' => ['-и', '-е', '-у', '-ой', '-е']
                        ],
                        [
                            'words' => ['ба', 'ва', 'да', 'жа', 'за', 'ла', 'ма', 'на', 'па', 'ра', 'са', 'та', 'фа', 'ца'],
                            'graduation' => ['-ы', '-е', '-у', '-ой', '-е']
                        ],
                        [
                            'words' => ['я'],
                            'graduation' => ['-и', '-е', '-ю', '-ей', '-е']
                        ],
                        [
                            'words' => ['и'],
                            'graduation' => ['--ок', '-ам', '.', '-ами', '-ах']
                        ],
                    ]
                ]
            ],
            self::GROUP_SECOND => [
                self::TYPE_ADJECTIVE => [
                    'exceptions' => [
                        [
                            'words' => ['алексеевское'],
                            'graduation' => ['--ого', '--ому', '--ое', '--им', '--ом']
                        ],
                    ],
                    'suffixes' => [
                        [
                            'words' => ['шое', 'шый', 'шой'],
                            'graduation' => ['--ого', '--ому', '.', '--им', '--ом']
                        ],
                        [
                            'words' => ['ое', 'ый', 'ой'],
                            'graduation' => ['--ого', '--ому', '.', '--ым', '--ом']
                        ],
                        [
                            'words' => ['ее', 'ий'],
                            'graduation' => ['--его', '--ему', '.', '--им', '--ем']
                        ],
                    ],
                ],
                self::TYPE_NONE => [
                    'exceptions' => [
                        [
                            'words' => ['галицино'],
                            'graduation' => ['.', '.', '.', '.', '.']
                        ],
                    ],
                    'suffixes' => [
                        [
                            'words' => ['б', 'в', 'г', 'д', 'ж', 'з', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'щ'],
                            'graduation' => ['а', 'у', '.', 'ом', 'е']
                        ],
                        [
                            'words' => ['ш'],
                            'graduation' => ['а', 'у', '.', 'ем', 'е']
                        ],
                        [
                            'words' => ['о'],
                            'graduation' => ['-а', '-у', '.', 'м', '-е']
                        ],
                        [
                            'words' => ['е'],
                            'graduation' => ['-я', '-ю', '.', 'м', '.']
                        ],
                        [
                            'words' => ['ы'],
                            'graduation' => ['-ов', '-ам', '.', '-ами', '-ах']
                        ],
                        [
                            'words' => ['ий'],
                            'graduation' => ['-я', '-ю', '.', '-ем', '-и']
                        ],
                        [
                            'words' => ['ь'],
                            'graduation' => ['-я', '-ю', '.', '-ем', '-е']
                        ],
                    ],
                ]
            ],
            self::GROUP_THIRD => [
                self::TYPE_ADJECTIVE => [
                    'suffixes' => [
                        [
                            'words' => ['ая'],
                            'graduation' => ['--ой', '--ой', '--ую', '--ой', '--ой']
                        ],
                        [
                            'words' => ['яя'],
                            'graduation' => ['--ей', '--ей', '--юю', '--ей', '--ей']
                        ],
                        [
                            'words' => ['ые', 'ие'],
                            'graduation' => ['-х', '-м', '.', '-ми', '-х']
                        ],
                    ],
                ],
                self::TYPE_NONE => [
                    'suffixes' => [
                        [
                            'words' => ['ь'],
                            'graduation' => ['-и', '-и', '.', '-ью', '-и']
                        ],
                        [
                            'words' => ['мя'],
                            'graduation' => ['-ени', '--ени', '.', '-енем', '-ени']
                        ],
                    ],
                ]
            ]
        ];
    }

    /**
     *
     */
    public function getGroup()
    {
        if (preg_match("/[а|я|и]$/", $this->word)) {
            $this->group = self::GROUP_FIRST;
        } elseif (preg_match("/[о|е|ы|вль|поль|коль|куль|прель|обль|чутль|раль|варь|брь|пароль]$/", $this->word)) {
            $this->group = self::GROUP_SECOND;
        } elseif (preg_match("/[мя|ь]$/", $this->word)) {
            $this->group = self::GROUP_THIRD;
        } else {
            $this->group = self::GROUP_SECOND;
        }
    }

    /**
     *
     */
    public function getType()
    {
        if (preg_match("/[ая|яя|ые|ие|ый|ой|[^р]ий|ое|ее]$/", $this->word)) {
            $this->type = self::TYPE_ADJECTIVE;
        } else {
            $this->type = self::TYPE_NONE;
        }
    }

    /**
     * @return mixed
     */
    public function getRulesByTypeAndGroup()
    {
        $rules = $this->getRules();
        $this->getGroup();
        $this->getType();

        return $rules[$this->group][$this->type];
    }

    /**
     * @return bool
     */
    public function processRules()
    {
        $rules = $this->getRulesByTypeAndGroup();
        if (isset($rules['exceptions'])) {
            foreach ($rules['exceptions'] as $item) {
                if (in_array($this->word, $item['words'])) {
                    return $item['graduation'];
                }
            }
        }

        foreach ($rules['suffixes'] as $item) {
            foreach ($item['words'] as $word) {
                if (preg_match("/$word$/", $this->word)) {
                    return $item['graduation'];
                }
            }
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function processWord()
    {
        $graduation = $this->processRules();
        if (!$graduation) {
            return false;
        }

        $result = [];
        foreach ($graduation as $single) {
            if ($single == '.') {
                $result[] = $this->word;
                continue;
            }
            $count_defis = substr_count($single, '-');
            $single = str_replace('-', '', $single);
            $result[] = substr($this->word, 0, strlen($this->word) - $count_defis) . $single;
        }

        return $result;
    }
}