<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;


$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                ],
                [
                    'attribute' => 'user_ip',
                ],
                [
                    'attribute' => 'word',
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:Y-m-d'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'dateFrom',
                        'attribute2' => 'dateTo',
                        'options' => ['placeholder' => 'Start date'],
                        'options2' => ['placeholder' => 'End date'],
                        'type' => DatePicker::TYPE_RANGE,

                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
                        ]
                    ])
                ]
            ],
        ]);
    ?>

</div>
