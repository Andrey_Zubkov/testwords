<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Склонение слов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'word') ?>

    <div class="form-group">
        <?= Html::submitButton('Просклонять', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if (isset($result) && !empty($result)): ?>
        <?php foreach ($result as $item): ?>
            <p><?= $item ?></p>
        <?php endforeach; ?>
    <?php endif; ?>

</div>
