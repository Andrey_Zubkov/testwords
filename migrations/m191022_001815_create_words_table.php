
<?php
use yii\db\Migration;

class m191022_001815_create_words_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%words}}', [
            'id' => $this->primaryKey(),
            'user_ip' => $this->string()->notNull(),
            'word' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%words}}');
    }
}